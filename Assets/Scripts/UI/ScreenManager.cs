using System.Linq;
using UnityEngine;

namespace Scorewarrior.Test.UI
{
    public class ScreenManager : MonoBehaviour
    {
        [SerializeField] private StartBattleScreen _continueScreen, _replayScreen;
        [SerializeField] private Bootstrapper _bootstrapper;

        private void Start()
        {
            _continueScreen.gameObject.SetActive(true);
            
            _replayScreen.StartButton.onClick.AddListener(() =>
            {
                _bootstrapper.Battlefield?.Clear();
            });

            _bootstrapper.OnStartBattle += () =>
            {
                _bootstrapper.Battlefield.OnBattleEnded += () =>
                {
                    _replayScreen.gameObject.SetActive(true);
                };
            };
        }
    }
}
