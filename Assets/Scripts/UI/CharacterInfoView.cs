using System.Globalization;
using Scorewarrior.Test.Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scorewarrior.Test.UI
{
    public class CharacterInfoView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _characterNameText, _weaponModifiersText, _characterModifiersText, 
            _damageText, _fireRateText, _ammoText;
        [SerializeField] private Slider _healthBar, _armorBar;
        [SerializeField] private Image _healthBarFill;

        private Character _character;

        public RectTransform RectTransform { get; private set; }

        private void Awake()
        {
            RectTransform = GetComponent<RectTransform>();
        }

        public void Initialize(Character character, Color teamColor)
        {
            if (character == null)
            {
                return;
            }
            
            if (_character != null)
            {
                _character.Health.OnChanged -= _healthBar.SetValueWithoutNotify;
                _character.Armor.OnChanged -= _armorBar.SetValueWithoutNotify;
                character.Descriptor.MaxHealth.OnChanged -= SetHealthBarMaxValue;
                character.Descriptor.MaxArmor.OnChanged -= SetArmorBarMaxValue;
                character.OnDeath -= TurnOffOnDeath;
                
                character.Weapon.Ammo.OnChanged -= ShowAmmo;
                character.Weapon.Descriptor.Damage.OnChanged -= ShowDamage;
                character.Weapon.Descriptor.FireRate.OnChanged -= ShowFireRate;
                character.Weapon.Descriptor.ClipSize.OnChanged -= ShowAmmo;
            }

            _character = character;
            
            _characterNameText.text = character.Transform.name.Replace("(Clone)", "");

            _healthBarFill.color = teamColor;
            
            _healthBar.value = _healthBar.maxValue = character.Descriptor.MaxHealth;
            _armorBar.value = _armorBar.maxValue = character.Descriptor.MaxArmor;
            
            ShowAmmo(0);
            ShowDamage(character.Weapon.Descriptor.Damage);
            ShowFireRate(character.Weapon.Descriptor.FireRate);
            
            character.Health.OnChanged += _healthBar.SetValueWithoutNotify;
            character.Armor.OnChanged += _armorBar.SetValueWithoutNotify;
            character.Descriptor.MaxHealth.OnChanged += SetHealthBarMaxValue;
            character.Descriptor.MaxArmor.OnChanged += SetArmorBarMaxValue;
            character.OnDeath += TurnOffOnDeath;

            character.Weapon.Ammo.OnChanged += ShowAmmo;
            character.Weapon.Descriptor.Damage.OnChanged += ShowDamage;
            character.Weapon.Descriptor.FireRate.OnChanged += ShowFireRate;
            character.Weapon.Descriptor.ClipSize.OnChanged += ShowAmmo;

            _weaponModifiersText.text = _characterModifiersText.text = "";
            foreach (var pair in character.Weapon.Modifiers)
            {
                _weaponModifiersText.text += $"{pair.Key.Name.Replace("WeaponModifier", "")}\n";
            }
            foreach (var pair in character.Modifiers)
            {
                _characterModifiersText.text += $"{pair.Key.Name.Replace("CharacterModifier", "")}\n";
            }
        }

        private void SetHealthBarMaxValue(float value)
        {
            _healthBar.maxValue = value;
        }
        
        private void SetArmorBarMaxValue(float value)
        {
            _armorBar.maxValue = value;
        }

        private void TurnOffOnDeath()
        {
            gameObject.SetActive(false);
        }

        private void ShowDamage(float value)
        {
            _damageText.text = $"D: {value.ToString(CultureInfo.InvariantCulture)}";
        }
        
        private void ShowFireRate(float value)
        {
            _fireRateText.text = $"R: {value.ToString("0.##", CultureInfo.InvariantCulture)}";
        }
        
        private void ShowAmmo(uint value)
        {
            _ammoText.text = $"{_character.Weapon.Ammo}/{_character.Weapon.Descriptor.ClipSize}";
        }
    }
}
