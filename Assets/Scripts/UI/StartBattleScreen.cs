using UnityEngine;
using UnityEngine.UI;

namespace Scorewarrior.Test.UI
{
    public class StartBattleScreen : MonoBehaviour
    {
        [SerializeField] private Button _startButton;
        [SerializeField] private Bootstrapper _bootstrapper;

        public Button StartButton => _startButton;

        private void Start()
        {
            _startButton.onClick.AddListener(() =>
            {
                _bootstrapper.StartBattle();
                gameObject.SetActive(false);
            });
        }
    }
}
