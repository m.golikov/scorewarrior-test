using System.Collections.Generic;
using Scorewarrior.Test.Utility;
using UnityEngine;
using UnityEngine.Serialization;

namespace Scorewarrior.Test.UI
{
    public class BattlefieldCharacterInfosView : MonoBehaviour
    {
        [SerializeField] private Camera _viewCamera;
        [SerializeField] private CharacterInfoView _characterInfoViewPrefab;
        [SerializeField] private Bootstrapper _bootstrapper;
        [SerializeField] private Color[] _teamColors;
        [SerializeField] private Vector3 _infoPositionOffset;
        
        private GameObjectPool<CharacterInfoView> _characterInfoViewPool;
        private readonly List<CharacterInfoView> _usedCharacterInfoViews = new();

        private void Start()
        {
            var rectTransform = GetComponent<RectTransform>();
            
            _characterInfoViewPool = new GameObjectPool<CharacterInfoView>(_characterInfoViewPrefab, transform);
            _bootstrapper.OnStartBattle += () =>
            {
                var battlefield = _bootstrapper.Battlefield;
                if (battlefield == null)
                {
                    return;
                }

                foreach (var pair in battlefield.CharactersByTeam)
                {
                    var teamColor = _teamColors[pair.Key - 1];
                    foreach (var character in pair.Value)
                    {
                        var characterInfoView = _characterInfoViewPool.Get();
                        characterInfoView.Initialize(character, teamColor);

                        var screenPosition = RectTransformUtility.WorldToScreenPoint(
                            _viewCamera, character.Position + _infoPositionOffset);
                        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
                                rectTransform, screenPosition, null, out var localPoint))
                        {
                            characterInfoView.RectTransform.localPosition = localPoint;
                        }
                        
                        _usedCharacterInfoViews.Add(characterInfoView);
                    }
                }

                battlefield.OnBattleEnded += () =>
                {
                    foreach (var characterInfoView in _usedCharacterInfoViews)
                    {
                        _characterInfoViewPool.Return(characterInfoView);
                    }
                    _usedCharacterInfoViews.Clear();
                };
            };
        }
    }
}
