﻿using System;
using System.Collections.Generic;
using Scorewarrior.Test.Models;
using Scorewarrior.Test.Models.Modifiers;
using Scorewarrior.Test.Views;
using UnityEngine;
using UnityEngine.Events;

namespace Scorewarrior.Test
{
	public class Bootstrapper : MonoBehaviour
	{
		[SerializeField]
		private CharacterPrefab[] _characters;
		[SerializeField]
		private SpawnPoint[] _spawns;

		[Header("Modifier Presets")]
		[SerializeField] private BaseModifierFactory<Character>[] _characterModifiers;
		[SerializeField] private BaseModifierFactory<Weapon>[] _weaponModifiers;

		private Battlefield _battlefield;

		public Battlefield Battlefield => _battlefield;

		public event Action OnStartBattle;

		public void StartBattle()
		{
			Dictionary<uint, List<Vector3>> spawnPositionsByTeam = new Dictionary<uint, List<Vector3>>();
			foreach (SpawnPoint spawn in _spawns)
			{
				uint team = spawn.Team;
				if (spawnPositionsByTeam.TryGetValue(team, out List<Vector3> spawnPoints))
				{
					spawnPoints.Add(spawn.transform.position);
				}
				else
				{
					spawnPositionsByTeam.Add(team, new List<Vector3>{ spawn.transform.position });
				}
			}
			
			_battlefield = new Battlefield(spawnPositionsByTeam);
			_battlefield.Start(_characters, _characterModifiers, _weaponModifiers);
			
			OnStartBattle?.Invoke();
		}

		public void Update()
		{
			_battlefield?.Update(Time.deltaTime);
		}
	}
}