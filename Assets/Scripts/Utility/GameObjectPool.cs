using UnityEngine;

namespace Scorewarrior.Test.Utility
{
    public class GameObjectPool<TComponent> : BasePool<TComponent> where TComponent : Component
    {
        private TComponent _prefab;
        private Transform _parent;

        public GameObjectPool(TComponent prefab, Transform parent)
        {
            _prefab = prefab;
            _parent = parent;
        }

        public override TComponent Get()
        {
            var result = base.Get();
            result.gameObject.SetActive(true);
            return result;
        }

        public override bool Return(TComponent instance)
        {
            var result = base.Return(instance);
            if (result)
            {
                instance.gameObject.SetActive(false);
            }

            return result;
        }

        protected override TComponent GenerateObject()
        {
            var instance = Object.Instantiate(_prefab, _parent);
            instance.gameObject.SetActive(false);
            return instance;
        }
    }
}
