using System.Collections.Generic;

namespace Scorewarrior.Test.Utility
{
    public abstract class BaseModifiable<T> : Observable<T>
    {
        public delegate T ValueModifier(T value);
        
        private readonly List<ValueModifier> _modifiers = new();

        public BaseModifiable(T value) : base(value) {}

        public override T Get()
        {
            var value = GetUnmodifiedValue();

            T totalDelta = default;
            foreach (var modifier in _modifiers)
            {
                var modifiedValue = modifier(value);
                var delta = Sub(modifiedValue, value);
                totalDelta = Add(totalDelta, delta);
            }

            return Add(value, totalDelta);
        }

        public T GetUnmodifiedValue()
        {
            return base.Get();
        }

        public void AddValueModifier(ValueModifier modifier)
        {
            _modifiers.Add(modifier);
            Notify();
        }

        public void RemoveValueModifier(ValueModifier modifier)
        {
            _modifiers.Remove(modifier);
            Notify();
        }

        protected abstract T Add(T t1, T t2);
        protected abstract T Sub(T t1, T t2);
    }
    
    public class ModifiableFloat : BaseModifiable<float>
    {
        public ModifiableFloat(float value) : base(value) { }
        
        protected override float Add(float t1, float t2)
        {
            return t1 + t2;
        }

        protected override float Sub(float t1, float t2)
        {
            return t1 - t2;
        }
    }

    public class ModifiableUint : BaseModifiable<uint>
    {
        public ModifiableUint(uint value) : base(value) { }

        protected override uint Add(uint t1, uint t2)
        {
            return t1 + t2;
        }

        protected override uint Sub(uint t1, uint t2)
        {
            return t1 - t2;
        }
    }
}
