using System.Collections.Generic;

namespace Scorewarrior.Test.Utility
{
    public abstract class BasePool<T>
    {
        private readonly Stack<T> _freeObjects = new();
        private readonly HashSet<T> _usedObjects = new();

        public virtual T Get()
        {
            if (_freeObjects.Count == 0)
            {
                AddNewObject();
            }

            var instance = _freeObjects.Pop();
            _usedObjects.Add(instance);

            return instance;
        }

        public virtual bool Return(T instance)
        {
            if (!_usedObjects.Contains(instance))
            {
                return false;
            }

            _usedObjects.Remove(instance);
            _freeObjects.Push(instance);

            return true;
        }

        private void AddNewObject()
        {
            _freeObjects.Push(GenerateObject());
        }

        protected abstract T GenerateObject();
    }

    public class Pool<T> : BasePool<T> where T : new()
    {
        protected override T GenerateObject()
        {
            return new T();
        }
    }
}
