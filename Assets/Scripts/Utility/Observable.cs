using UnityEngine;
using UnityEngine.Events;

namespace Scorewarrior.Test.Utility
{
    public class Observable<T>
    {
        private T _value;

        public Observable(T value)
        {
            _value = value;
        }

        public T Value
        {
            get => Get();
            set => Set(value);
        }

        public delegate void OnChangedHandler(T value);
        public event OnChangedHandler OnChanged;

        public static implicit operator T(Observable<T> observable)
        {
            return observable.Value;
        }

        public virtual T Get()
        {
            return _value;
        }

        public virtual void Set(T value)
        {
            _value = value;
            Notify();
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        protected void Notify()
        {
            OnChanged?.Invoke(Value);
        }
    }
}
