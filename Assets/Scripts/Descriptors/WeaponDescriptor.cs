﻿using System;
using Scorewarrior.Test.Utility;
using UnityEngine;

namespace Scorewarrior.Test.Descriptors
{
	public class WeaponDescriptor : MonoBehaviour
	{
		[SerializeField] private float _damage;
		[SerializeField] private float _accuracy;
		[SerializeField] private float _fireRate;
		[SerializeField] private uint _clipSize;
		[SerializeField] private float _reloadTime;
		
		public ModifiableFloat Damage { get; private set; }
		public ModifiableFloat Accuracy { get; private set; }
		public ModifiableFloat FireRate { get; private set; }
		public ModifiableUint ClipSize { get; private set; }
		public ModifiableFloat ReloadTime { get; private set; }

		private void Awake()
		{
			Damage = new ModifiableFloat(_damage);
			Accuracy = new ModifiableFloat(_accuracy);
			FireRate = new ModifiableFloat(_fireRate);
			ClipSize = new ModifiableUint(_clipSize);
			ReloadTime = new ModifiableFloat(_reloadTime);
		}
	}
}