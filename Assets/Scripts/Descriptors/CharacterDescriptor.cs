﻿using System;
using Scorewarrior.Test.Utility;
using UnityEngine;

namespace Scorewarrior.Test.Descriptors
{
	public class CharacterDescriptor : MonoBehaviour
	{
		[SerializeField] private float _accuracy;
		[SerializeField] private float _dexterity;
		[SerializeField] private float _maxHealth;
		[SerializeField] private float _maxArmor;
		[SerializeField] private float _aimTime;
		
		public ModifiableFloat Accuracy { get; private set; }
		public ModifiableFloat Dexterity { get; private set; }
		public ModifiableFloat MaxHealth { get; private set; }
		public ModifiableFloat MaxArmor { get; private set; }
		public ModifiableFloat AimTime { get; private set; }

		private void Awake()
		{
			Accuracy = new ModifiableFloat(_accuracy);
			Dexterity = new ModifiableFloat(_dexterity);
			MaxHealth = new ModifiableFloat(_maxHealth);
			MaxArmor = new ModifiableFloat(_maxArmor);
			AimTime = new ModifiableFloat(_aimTime);
		}
	}
}