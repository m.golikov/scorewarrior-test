﻿using Scorewarrior.Test.Models;
using UnityEngine;

namespace Scorewarrior.Test.Views
{
	public class CharacterPrefab : BasePrefab<Character>
	{
		public WeaponPrefab Weapon;
		public Animator Animator;
		public float ReloadAnimationLength = 3.3f;

		[SerializeField]
		private Transform _rightPalm;

		protected override void Init(Character model)
		{
			model.OnStateChanged += state =>
			{
				switch (state)
				{
					case Character.State.Idle:
						Animator.SetBool("aiming", false);
						Animator.SetBool("reloading", false);
						break;
					case Character.State.Aiming:
						Animator.SetBool("aiming", true);
						Animator.SetBool("reloading", false);
						break;
					case Character.State.Shooting:
						Animator.SetBool("aiming", true);
						Animator.SetBool("reloading", false);
						break;
					case Character.State.Reloading:
						Animator.SetBool("aiming", true);
						Animator.SetBool("reloading", true);
						Animator.SetFloat("reload_time", ReloadAnimationLength / model.Weapon.Descriptor.ReloadTime);
						break;
				}
			};

			model.OnShoot += () =>
			{
				Animator.SetTrigger("shoot");
			};

			model.OnDeath += () =>
			{
				Animator.SetTrigger("die");
			};

			model.OnDestroy += () =>
			{
				Destroy(gameObject);
			};
		}

		public void Update()
		{
			if (_rightPalm != null && Weapon != null)
			{
				Weapon.transform.position = _rightPalm.position;
				Weapon.transform.forward = _rightPalm.up;
			}
		}
	}
}
