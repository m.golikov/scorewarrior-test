﻿using Scorewarrior.Test.Descriptors;
using Scorewarrior.Test.Models;
using UnityEngine;

namespace Scorewarrior.Test.Views
{
	public class BulletPrefab : BasePrefab<Bullet>
	{
		[SerializeField] private float _speed = 30, _missDistance = 3, _missMinOffset = 1, _missMaxOffset = 2;
		
		private Vector3 _position;
		private Vector3 _direction;
		private float _totalDistance;
		private float _currentDistance;

		protected override void Init(Bullet model)
		{
			_position = transform.position;
			var originalPosition = model.Target.Position + Vector3.up * 2.0f;
			var targetPosition = originalPosition + GetOffset(originalPosition - transform.position);
			_direction = Vector3.Normalize(targetPosition - transform.position);
			_totalDistance = Vector3.Distance(targetPosition, transform.position);
			_currentDistance = 0;
		}

		public void Update()
		{
			_currentDistance += Time.deltaTime * _speed;
			
			if (_currentDistance < _totalDistance * (Model.Hit ? 1 : _missDistance))
			{
				transform.position = _position + _currentDistance * _direction;
			}
			else
			{
				Model.HitCharacter();
				Destroy(gameObject);
			}
		}

		private Vector3 GetOffset(Vector3 direction)
		{
			return Model.Hit
				? Vector3.zero
				: Vector3.Cross(direction, Vector3.up).normalized * 
				  (_missMinOffset + Random.value * (_missMaxOffset - _missMinOffset)) * (Random.value > 0.5f ? 1 : -1);
		}
	}
}