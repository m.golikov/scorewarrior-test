﻿using Scorewarrior.Test.Models;
using UnityEngine;

namespace Scorewarrior.Test.Views
{
	public class WeaponPrefab : BasePrefab<Weapon>
	{
		public Transform BarrelTransform;

		[SerializeField]
		private GameObject _bulletPrefab;

		protected override void Init(Weapon model)
		{
			model.OnFire += Fire;
		}

		public void Fire(Bullet bullet)
		{
			GameObject bulletObject = Instantiate(_bulletPrefab, null);
			BulletPrefab bulletPrefab = bulletObject.GetComponent<BulletPrefab>();
			bulletPrefab.transform.position = BarrelTransform.position;
			bulletPrefab.Initialize(bullet);
		}
	}
}