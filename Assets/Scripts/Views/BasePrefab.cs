using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasePrefab<TModel> : MonoBehaviour
{
    public TModel Model { get; private set; }

    public void Initialize(TModel model)
    {
        Model = model;
        Init(Model);
    }
    
    protected abstract void Init(TModel model);
}
