using Scorewarrior.Test.Views;
using UnityEditor;
using UnityEngine;

namespace Scorewarrior.Test.Views.Editor
{
    [CustomEditor(typeof(CharacterPrefab))]
    public class CharacterPrefabEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var characterPrefab = target as CharacterPrefab;

            if (characterPrefab && characterPrefab.Model != null)
            {
                GUILayout.Space(10);
                
                GUILayout.Label("Character data");
                GUILayout.Label($"Health: {characterPrefab.Model.Health}/{characterPrefab.Model.Descriptor.MaxHealth}" +
                                $"({characterPrefab.Model.Descriptor.MaxHealth.GetUnmodifiedValue()})");
                GUILayout.Label($"Armor: {characterPrefab.Model.Armor}/{characterPrefab.Model.Descriptor.MaxArmor}" +
                                $"({characterPrefab.Model.Descriptor.MaxArmor.GetUnmodifiedValue()})");
                GUILayout.Label($"Dexterity: {characterPrefab.Model.Descriptor.Dexterity}({characterPrefab.Model.Descriptor.Dexterity.GetUnmodifiedValue()})");
                GUILayout.Label($"Accuracy: {characterPrefab.Model.Descriptor.Accuracy}({characterPrefab.Model.Descriptor.Accuracy.GetUnmodifiedValue()})");
                GUILayout.Label($"AimTime: {characterPrefab.Model.Descriptor.AimTime}({characterPrefab.Model.Descriptor.AimTime.GetUnmodifiedValue()})");
                GUILayout.Label($"Alive: {characterPrefab.Model.IsAlive}");
                GUILayout.Label($"State: {characterPrefab.Model.CharacterState}");
                
                GUILayout.Space(10);
                
                GUILayout.Label("Weapon data");
                GUILayout.Label($"Damage: {characterPrefab.Model.Weapon.Descriptor.Damage}({characterPrefab.Model.Weapon.Descriptor.Damage.GetUnmodifiedValue()})");
                GUILayout.Label($"Accuracy: {characterPrefab.Model.Weapon.Descriptor.Accuracy}({characterPrefab.Model.Weapon.Descriptor.Accuracy.GetUnmodifiedValue()})");
                GUILayout.Label($"FireRate: {characterPrefab.Model.Weapon.Descriptor.FireRate}({characterPrefab.Model.Weapon.Descriptor.FireRate.GetUnmodifiedValue()})");
                GUILayout.Label($"ReloadTime: {characterPrefab.Model.Weapon.Descriptor.ReloadTime}({characterPrefab.Model.Weapon.Descriptor.ReloadTime.GetUnmodifiedValue()})");
                GUILayout.Label($"Ammo: {characterPrefab.Model.Weapon.Ammo}/{characterPrefab.Model.Weapon.Descriptor.ClipSize}" +
                                $"({characterPrefab.Model.Weapon.Descriptor.ClipSize.GetUnmodifiedValue()})");
            }
        }
    }
}
