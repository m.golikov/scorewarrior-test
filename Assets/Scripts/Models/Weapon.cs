﻿using Scorewarrior.Test.Descriptors;
using Scorewarrior.Test.Utility;

namespace Scorewarrior.Test.Models
{
	public class Weapon : ModifiableModel<Weapon, WeaponDescriptor>
	{
		private bool _ready;
		private float _time;

		public Weapon(WeaponDescriptor weaponDescriptor) : base(weaponDescriptor)
		{
			Ammo = new Observable<uint>(weaponDescriptor.ClipSize);
		}
		
		public Observable<uint> Ammo { get; }

		public bool IsReady => _ready;
		public bool HasAmmo => Ammo > 0;
		
		public delegate void OnBulletFiredHandler(Bullet bullet);
		public event OnBulletFiredHandler OnFire;

		public void Reload()
		{
			Ammo.Value = Descriptor.ClipSize;
		}

		public void Fire(Character character, bool hit)
		{
			if (HasAmmo)
			{
				Ammo.Value -= 1;
				OnFire?.Invoke(new Bullet(character, this, hit));
				_time = 1.0f / Descriptor.FireRate;
				_ready = false;
			}
		}

		public void Update(float deltaTime)
		{
			if (!_ready)
			{
				if (_time > 0)
				{
					_time -= deltaTime;
				}
				else
				{
					_ready = true;
				}
			}
		}
	}
}