using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Scorewarrior.Test.Models;
using Scorewarrior.Test.Models.Modifiers;

namespace Scorewarrior.Test.Models
{
    public abstract class ModifiableModel<TSelf, TDescriptor> : Describable<TDescriptor> where TSelf : ModifiableModel<TSelf, TDescriptor>
    {
        private Dictionary<Type, BaseModifier<TSelf>> _modifiers = new();

        public ModifiableModel(TDescriptor descriptor) : base(descriptor)
        {
            Modifiers = new ReadOnlyDictionary<Type, BaseModifier<TSelf>>(_modifiers);
        }

        public ReadOnlyDictionary<Type, BaseModifier<TSelf>> Modifiers { get; }
        
        public void AddModifier(BaseModifier<TSelf> modifier)
        {
            if (modifier.ModifiedModel != null)
            {
                return;
            }
            
            modifier.AddToModel((TSelf) this);
            _modifiers.Add(modifier.GetType(), modifier);
        }

        public void RemoveModifier(Type modifierType)
        {
            if (_modifiers.TryGetValue(modifierType, out var modifier))
            {
                modifier.RemoveFromModel();
                _modifiers.Remove(modifierType);
            }
        }
    }
}
