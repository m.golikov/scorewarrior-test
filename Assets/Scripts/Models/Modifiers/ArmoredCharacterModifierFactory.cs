using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class ArmoredCharacterModifierFactory : BaseModifierFactory<Character>
    {
        [SerializeField] private float _dexterityFactor, _accuracyFactor, _maxArmorFactor;
        
        public override BaseModifier<Character> CreateModifier()
        {
            return new ArmoredCharacterModifier(_dexterityFactor, _accuracyFactor, _maxArmorFactor);
        }
    }
    
    public class ArmoredCharacterModifier : BaseModifier<Character>
    {
        private readonly float _dexterityFactor, _accuracyFactor, _maxArmorFactor;

        public ArmoredCharacterModifier(float dexterityFactor, float accuracyFactor, float maxArmorFactor)
        {
            _dexterityFactor = dexterityFactor;
            _accuracyFactor = accuracyFactor;
            _maxArmorFactor = maxArmorFactor;
        }

        protected override void ModifyModel(Character model)
        {
            model.Descriptor.Dexterity.AddValueModifier(ModifyDexterity);
            model.Descriptor.Accuracy.AddValueModifier(ModifyAccuracy);
            model.Descriptor.MaxArmor.AddValueModifier(ModifyMaxArmor);
            model.Armor.Value = model.Descriptor.MaxArmor;
        }

        protected override void RemoveModifiers(Character model)
        {
            model.Descriptor.Dexterity.RemoveValueModifier(ModifyDexterity);
            model.Descriptor.Accuracy.RemoveValueModifier(ModifyAccuracy);
            model.Descriptor.MaxArmor.RemoveValueModifier(ModifyMaxArmor);
        }

        private float ModifyDexterity(float dexterity)
        {
            return dexterity * _dexterityFactor;
        }

        private float ModifyAccuracy(float accuracy)
        {
            return accuracy * _accuracyFactor;
        }

        private float ModifyMaxArmor(float maxArmor)
        {
            return maxArmor * _maxArmorFactor;
        }
    }
}
