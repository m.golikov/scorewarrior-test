using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class FastHandsCharacterModifierFactory : BaseModifierFactory<Character>
    {
        [SerializeField] private float _aimTimeFactor, _reloadTimeFactor;
        
        public override BaseModifier<Character> CreateModifier()
        {
            return new FastHandsCharacterModifier(_aimTimeFactor, _reloadTimeFactor);
        }
    }
    
    public class FastHandsCharacterModifier : BaseModifier<Character>
    {
        private readonly float _aimTimeFactor, _reloadTimeFactor;

        public FastHandsCharacterModifier(float aimTimeFactor, float reloadTimeFactor)
        {
            _aimTimeFactor = aimTimeFactor;
            _reloadTimeFactor = reloadTimeFactor;
        }

        protected override void ModifyModel(Character model)
        {
            model.Descriptor.AimTime.AddValueModifier(ModifyAimTime);
            model.Weapon.Descriptor.ReloadTime.AddValueModifier(ModifyReloadTime);
        }

        protected override void RemoveModifiers(Character model)
        {
            model.Descriptor.AimTime.RemoveValueModifier(ModifyAimTime);
            model.Weapon.Descriptor.ReloadTime.RemoveValueModifier(ModifyReloadTime);
        }

        private float ModifyAimTime(float aimTime)
        {
            return aimTime * _aimTimeFactor;
        }

        private float ModifyReloadTime(float reloadTime)
        {
            return reloadTime * _reloadTimeFactor;
        }
    }
}