using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public abstract class BaseModifierFactory<TModel> : MonoBehaviour
    {
        public abstract BaseModifier<TModel> CreateModifier();
    }
    
    public abstract class BaseModifier<TModel>
    {
        private TModel _modifiedModel;

        public TModel ModifiedModel => _modifiedModel;

        public void AddToModel(TModel model)
        {
            if (_modifiedModel != null)
            {
                return;
            }
            
            _modifiedModel = model;
            ModifyModel(model);
        }

        public void RemoveFromModel()
        {
            if (_modifiedModel == null)
            {
                return;
            }
            
            RemoveModifiers(_modifiedModel);
            _modifiedModel = default;
        }
        
        protected abstract void ModifyModel(TModel model);
        protected abstract void RemoveModifiers(TModel model);
    }
}
