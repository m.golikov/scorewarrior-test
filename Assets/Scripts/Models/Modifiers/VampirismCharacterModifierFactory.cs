using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class VampirismCharacterModifierFactory : BaseModifierFactory<Character>
    {
        [SerializeField] private float _vampirismFactor = 0.2f;
        
        public override BaseModifier<Character> CreateModifier()
        {
            return new VampirismCharacterModifier(_vampirismFactor);
        }
    }
    
    public class VampirismCharacterModifier : BaseModifier<Character>
    {
        private readonly float _vampirismFactor;

        public VampirismCharacterModifier(float vampirismFactor)
        {
            _vampirismFactor = vampirismFactor;
        }

        protected override void ModifyModel(Character model)
        {
            model.Weapon.OnFire += AddVampirismToBullet;
        }

        protected override void RemoveModifiers(Character model)
        {
            model.Weapon.OnFire -= AddVampirismToBullet;
        }

        private void AddVampirismToBullet(Bullet bullet)
        {
            bullet.OnHit += damage =>
            {
                if (!ModifiedModel.IsAlive)
                {
                    return;
                }
                
                ModifiedModel.Health.Value += damage * _vampirismFactor;
                ModifiedModel.Health.Value =
                    Mathf.Clamp(ModifiedModel.Health.Value, 0, ModifiedModel.Descriptor.MaxHealth);
            };
        }
    }
}
