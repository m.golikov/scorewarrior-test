using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class TankCharacterModifierFactory : BaseModifierFactory<Character>
    {
        [SerializeField] private float _fireRateFactor, _maxHealthFactor;
        
        public override BaseModifier<Character> CreateModifier()
        {
            return new TankCharacterModifier(_fireRateFactor, _maxHealthFactor);
        }
    }
    
    public class TankCharacterModifier : BaseModifier<Character>
    {
        private readonly float _fireRateFactor, _maxHealthFactor;

        public TankCharacterModifier(float fireRateFactor, float maxHealthFactor)
        {
            _fireRateFactor = fireRateFactor;
            _maxHealthFactor = maxHealthFactor;
        }

        protected override void ModifyModel(Character model)
        {
            model.Weapon.Descriptor.FireRate.AddValueModifier(ModifyFireRate);
            model.Descriptor.MaxHealth.AddValueModifier(ModifyMaxHealth);
            model.Health.Value = model.Descriptor.MaxHealth;
        }

        protected override void RemoveModifiers(Character model)
        {
            model.Weapon.Descriptor.FireRate.RemoveValueModifier(ModifyFireRate);
            model.Descriptor.MaxHealth.RemoveValueModifier(ModifyMaxHealth);
        }

        private float ModifyFireRate(float fireRate)
        {
            return fireRate * _fireRateFactor;
        }

        private float ModifyMaxHealth(float maxHealth)
        {
            return maxHealth * _maxHealthFactor;
        }
    }
}
