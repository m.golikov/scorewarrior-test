using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class PerfectWeaponWeaponModifierFactory : BaseModifierFactory<Weapon>
    {
        [SerializeField] private float _accuracyFactor, _reloadTimeFactor, _fireRateFactor;
        
        public override BaseModifier<Weapon> CreateModifier()
        {
            return new PerfectWeaponWeaponModifier(_accuracyFactor, _reloadTimeFactor, _fireRateFactor);
        }
    }
    
    public class PerfectWeaponWeaponModifier : BaseModifier<Weapon>
    {
        private readonly float _accuracyFactor, _reloadTimeFactor, _fireRateFactor;

        public PerfectWeaponWeaponModifier(float accuracyFactor, float reloadTimeFactor, float fireRateFactor)
        {
            _accuracyFactor = accuracyFactor;
            _reloadTimeFactor = reloadTimeFactor;
            _fireRateFactor = fireRateFactor;
        }

        protected override void ModifyModel(Weapon model)
        {
            model.Descriptor.Accuracy.AddValueModifier(ModifyAccuracy);
            model.Descriptor.ReloadTime.AddValueModifier(ModifyReloadTime);
            model.Descriptor.FireRate.AddValueModifier(ModifyFireRate);
        }

        protected override void RemoveModifiers(Weapon model)
        {
            model.Descriptor.Accuracy.RemoveValueModifier(ModifyAccuracy);
            model.Descriptor.ReloadTime.RemoveValueModifier(ModifyReloadTime);
            model.Descriptor.FireRate.RemoveValueModifier(ModifyFireRate);
        }

        private float ModifyAccuracy(float accuracy)
        {
            return accuracy * _accuracyFactor;
        }

        private float ModifyReloadTime(float reloadTime)
        {
            return reloadTime * _reloadTimeFactor;
        }
        
        private float ModifyFireRate(float fireRate)
        {
            return fireRate * _fireRateFactor;
        }
    }
}