using System.Collections.Generic;
using Scorewarrior.Test.Utility;
using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class CriticalHitWeaponModifierFactory : BaseModifierFactory<Weapon>
    {
        [SerializeField] private float _criticalHitFactor, _criticalHitChance;
        
        public override BaseModifier<Weapon> CreateModifier()
        {
            return new CriticalHitWeaponModifier(_criticalHitFactor, _criticalHitChance);
        }
    }

    public class CriticalHitWeaponModifier : BaseModifier<Weapon>
    {
        private readonly float _criticalHitFactor, _criticalHitChance;

        private HashSet<BaseModifiable<float>.ValueModifier> _criticalHits = new();
        
        public CriticalHitWeaponModifier(float criticalHitFactor, float criticalHitChance)
        {
            _criticalHitFactor = criticalHitFactor;
            _criticalHitChance = criticalHitChance;
        }

        protected override void ModifyModel(Weapon model)
        {
            model.OnFire += PredictCritical;
        }

        protected override void RemoveModifiers(Weapon model)
        {
            foreach (var valueModifier in _criticalHits)
            {
                ModifiedModel.Descriptor.Damage.RemoveValueModifier(valueModifier);
            }
            _criticalHits.Clear();

            model.OnFire -= PredictCritical;
        }

        private void PredictCritical(Bullet bullet)
        {
            var isCritical = Random.value < _criticalHitChance;
            
            if (isCritical)
            {
                BaseModifiable<float>.ValueModifier criticalModifier = damage => damage * 
                    (_criticalHits.Count > 0 ? 1 : _criticalHitFactor);
                ModifiedModel.Descriptor.Damage.AddValueModifier(criticalModifier);
                _criticalHits.Add(criticalModifier);
                bullet.OnHit += damage =>
                {
                    ModifiedModel.Descriptor.Damage.RemoveValueModifier(criticalModifier);
                    _criticalHits.Remove(criticalModifier);
                };
            }
        }
    }
}
