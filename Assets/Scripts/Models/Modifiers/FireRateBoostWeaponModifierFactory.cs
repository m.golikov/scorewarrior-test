using Scorewarrior.Test.Utility;
using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class FireRateBoostWeaponModifierFactory : BaseModifierFactory<Weapon>
    {
        [SerializeField] private float _boostPerFireFactor = 0.05f;
        
        public override BaseModifier<Weapon> CreateModifier()
        {
            return new FireRateBoostWeaponModifier(_boostPerFireFactor);
        }
    }
    
    public class FireRateBoostWeaponModifier : BaseModifier<Weapon>
    {
        private readonly float _boostPerFireFactor;
        
        private BaseModifiable<float>.ValueModifier _boostModifier;
        private float _boost;

        public FireRateBoostWeaponModifier(float boostPerFireFactor)
        {
            _boostPerFireFactor = boostPerFireFactor;
        }

        protected override void ModifyModel(Weapon model)
        {
            model.OnFire += BoostFireRate;
        }

        protected override void RemoveModifiers(Weapon model)
        {
            ModifiedModel.Descriptor.FireRate.RemoveValueModifier(_boostModifier);
            model.OnFire -= BoostFireRate;
        }

        private void BoostFireRate(Bullet bullet)
        {
            var boost = _boost += _boostPerFireFactor / Mathf.Clamp(ModifiedModel.Descriptor.FireRate, 1, float.MaxValue);
            ModifiedModel.Descriptor.FireRate.RemoveValueModifier(_boostModifier);
            _boostModifier = fireRate => fireRate + boost;
            ModifiedModel.Descriptor.FireRate.AddValueModifier(_boostModifier);
        }
    }
}
