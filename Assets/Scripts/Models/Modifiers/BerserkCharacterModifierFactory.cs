using Scorewarrior.Test.Utility;
using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class BerserkCharacterModifierFactory : BaseModifierFactory<Character>
    {
        [SerializeField] private float _maxFireRateBoost = 2;
        
        public override BaseModifier<Character> CreateModifier()
        {
            return new BerserkCharacterModifier(_maxFireRateBoost);
        }
    }
    
    public class BerserkCharacterModifier : BaseModifier<Character>
    {
        private readonly float _maxFireRateBoost;

        private BaseModifiable<float>.ValueModifier _boostModifier;
        private float _boost;

        public BerserkCharacterModifier(float maxFireRateBoost)
        {
            _maxFireRateBoost = maxFireRateBoost;
        }

        protected override void ModifyModel(Character model)
        {
            model.Health.OnChanged += BoostFireRate;
            BoostFireRate(model.Health);
        }

        protected override void RemoveModifiers(Character model)
        {
            ModifiedModel.Weapon.Descriptor.FireRate.RemoveValueModifier(_boostModifier);
            model.Health.OnChanged -= BoostFireRate;
        }

        private void BoostFireRate(float health)
        {
            ModifiedModel.Weapon.Descriptor.FireRate.RemoveValueModifier(_boostModifier);
            var boost = Mathf.Lerp(_maxFireRateBoost, 1, health / ModifiedModel.Descriptor.MaxHealth);
            _boostModifier = fireRate => fireRate * boost;
            ModifiedModel.Weapon.Descriptor.FireRate.AddValueModifier(_boostModifier);
        }
    }
}
