using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class GlassCannonCharacterModifierFactory : BaseModifierFactory<Character>
    {
        [SerializeField] private float _damageFactor, _maxHealthFactor;
        
        public override BaseModifier<Character> CreateModifier()
        {
            return new GlassCannonCharacterModifier(_damageFactor, _maxHealthFactor);
        }
    }
    
    public class GlassCannonCharacterModifier : BaseModifier<Character>
    {
        private readonly float _damageFactor, _maxHealthFactor;

        public GlassCannonCharacterModifier(float damageFactor, float maxHealthFactor)
        {
            _damageFactor = damageFactor;
            _maxHealthFactor = maxHealthFactor;
        }

        protected override void ModifyModel(Character model)
        {
            model.Weapon.Descriptor.Damage.AddValueModifier(ModifyDamage);
            model.Descriptor.MaxHealth.AddValueModifier(ModifyMaxHealth);
            model.Health.Value = model.Descriptor.MaxHealth;
        }

        protected override void RemoveModifiers(Character model)
        {
            model.Weapon.Descriptor.Damage.RemoveValueModifier(ModifyDamage);
            model.Descriptor.MaxHealth.RemoveValueModifier(ModifyMaxHealth);
        }

        private float ModifyDamage(float damage)
        {
            return damage * _damageFactor;
        }

        private float ModifyMaxHealth(float maxHealth)
        {
            return maxHealth * _maxHealthFactor;
        }
    }
}
