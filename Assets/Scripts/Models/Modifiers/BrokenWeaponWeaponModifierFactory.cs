using UnityEngine;

namespace Scorewarrior.Test.Models.Modifiers
{
    public class BrokenWeaponWeaponModifierFactory : BaseModifierFactory<Weapon>
    {
        [SerializeField] private float _clipSizeFactor, _reloadTimeFactor;
        
        public override BaseModifier<Weapon> CreateModifier()
        {
            return new BrokenWeaponWeaponModifier(_clipSizeFactor, _reloadTimeFactor);
        }
    }
    
    public class BrokenWeaponWeaponModifier : BaseModifier<Weapon>
    {
        private readonly float _clipSizeFactor, _reloadTimeFactor;

        public BrokenWeaponWeaponModifier(float clipSizeFactor, float reloadTimeFactor)
        {
            _clipSizeFactor = clipSizeFactor;
            _reloadTimeFactor = reloadTimeFactor;
        }

        protected override void ModifyModel(Weapon model)
        {
            model.Descriptor.ClipSize.AddValueModifier(ModifyClipSize);
            model.Descriptor.ReloadTime.AddValueModifier(ModifyReloadTime);
            model.Ammo.Value = model.Descriptor.ClipSize;
        }

        protected override void RemoveModifiers(Weapon model)
        {
            model.Descriptor.ClipSize.RemoveValueModifier(ModifyClipSize);
            model.Descriptor.ReloadTime.RemoveValueModifier(ModifyReloadTime);
        }

        private uint ModifyClipSize(uint clipSize)
        {
            return (uint) (clipSize * _clipSizeFactor);
        }

        private float ModifyReloadTime(float reloadTime)
        {
            return reloadTime * _reloadTimeFactor;
        }
    }
}
