﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Scorewarrior.Test.Descriptors;
using Scorewarrior.Test.Models.Modifiers;
using Scorewarrior.Test.Views;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Scorewarrior.Test.Models
{
	public class Battlefield
	{
		private readonly Dictionary<uint, List<Vector3>> _spawnPositionsByTeam;
		private readonly Dictionary<uint, List<Character>> _charactersByTeam;

		private bool _paused;

		public ReadOnlyDictionary<uint, List<Character>> CharactersByTeam
		{
			get;
			private set;
		}

		public event Action OnBattleEnded;

		public Battlefield(Dictionary<uint, List<Vector3>> spawnPositionsByTeam)
		{
			_spawnPositionsByTeam = spawnPositionsByTeam;
			_charactersByTeam = new Dictionary<uint, List<Character>>();
			CharactersByTeam = new ReadOnlyDictionary<uint, List<Character>>(_charactersByTeam);
		}

		public void Start(CharacterPrefab[] prefabs, 
			BaseModifierFactory<Character>[] characterModifiers, 
			BaseModifierFactory<Weapon>[] weaponModifiers)
		{
			_paused = false;
			_charactersByTeam.Clear();

			List<CharacterPrefab> availablePrefabs = new List<CharacterPrefab>(prefabs);
			foreach (var positionsPair in _spawnPositionsByTeam)
			{
				List<Vector3> positions = positionsPair.Value;
				List<Character> characters = new List<Character>();
				_charactersByTeam.Add(positionsPair.Key, characters);
				int i = 0;
				while (i < positions.Count && availablePrefabs.Count > 0)
				{
					int index = Random.Range(0, availablePrefabs.Count);
					var character = CreateCharacterAt(availablePrefabs[index], this, positions[i]);
					AddModifiers(character, characterModifiers, weaponModifiers);
					characters.Add(character);
					availablePrefabs.RemoveAt(index);
					i++;
				}
			}
			
			foreach (var characters in _charactersByTeam)
			{
				foreach (var character in characters.Value)
				{
					character.OnDeath += () =>
					{
						if (!IsAllTeamsAlive())
						{
							OnBattleEnded?.Invoke();
						}
					};
				}
			}
		}
		
		private bool IsAllTeamsAlive()
		{
			return _charactersByTeam
				.Select(characters => characters.Value
					.Select(character => character.IsAlive)
					.Aggregate((acc, val) => acc || val))
				.Aggregate((acc, val) => acc && val);
		}

		public bool TryGetNearestAliveEnemy(Character character, out Character target)
		{
			if (TryGetTeam(character, out uint team))
			{
				Character nearestEnemy = null;
				float nearestDistance = float.MaxValue;
				List<Character> enemies = team == 1 ? _charactersByTeam[2] : _charactersByTeam[1];
				foreach (Character enemy in enemies)
				{
					if (enemy.IsAlive)
					{
						float distance = Vector3.Distance(character.Position, enemy.Position);
						if (distance < nearestDistance)
						{
							nearestDistance = distance;
							nearestEnemy = enemy;
						}
					}
				}
				target = nearestEnemy;
				return target != null;
			}
			target = default;
			return false;
		}

		public bool TryGetTeam(Character target, out uint team)
		{
			foreach (var charactersPair in _charactersByTeam)
			{
				List<Character> characters = charactersPair.Value;
				foreach (Character character in characters)
				{
					if (character == target)
					{
						team = charactersPair.Key;
						return true;
					}
				}
			}
			team = default;
			return false;
		}

		public void Update(float deltaTime)
		{
			if (!_paused)
			{
				foreach (var charactersPair in _charactersByTeam)
				{
					List<Character> characters = charactersPair.Value;
					foreach (Character character in characters)
					{
						character.Update(deltaTime);
					}
				}
			}
		}

		public void Clear()
		{
			foreach (var characters in _charactersByTeam.Values)
			{
				foreach (var character in characters)
				{
					character.Destroy();
				}
			}
			_charactersByTeam.Clear();
		}

		private static Character CreateCharacterAt(CharacterPrefab prefab, Battlefield battlefield, Vector3 position)
		{
			CharacterPrefab characterPrefab = Object.Instantiate(prefab);
			characterPrefab.transform.position = position;
			
			var weapon = new Weapon(characterPrefab.Weapon.GetComponent<WeaponDescriptor>());
			characterPrefab.Weapon.Initialize(weapon);
			
			var character = new Character(characterPrefab.GetComponent<CharacterDescriptor>(), 
				characterPrefab.transform, weapon, battlefield);
			characterPrefab.Initialize(character);

			return character;
		}

		private static void AddModifiers(Character character, 
			BaseModifierFactory<Character>[] characterModifierFactories, 
			BaseModifierFactory<Weapon>[] weaponModifierFactories)
		{
			var characterFactories = characterModifierFactories.ToList();
			var weaponFactories = weaponModifierFactories.ToList();

			for (var i = 0; i < 3; i++)
			{
				var characterModifierFactory = characterFactories[Random.Range(0, characterFactories.Count)];
				character.AddModifier(characterModifierFactory.CreateModifier());
				characterFactories.Remove(characterModifierFactory);
			}

			for (var i = 0; i < 2; i++)
			{
				var weaponModifierFactory = weaponFactories[Random.Range(0, weaponFactories.Count)];
				character.Weapon.AddModifier(weaponModifierFactory.CreateModifier());
				weaponFactories.Remove(weaponModifierFactory);
			}
		}
	}
}