using System;
using Scorewarrior.Test.Descriptors;

namespace Scorewarrior.Test.Models
{
    public class Bullet
    {
        private readonly Character _target;
        private readonly Weapon _weapon;
        private readonly bool _hit;

        public Bullet(Character target, Weapon weapon, bool hit)
        {
            _target = target;
            _weapon = weapon;
            _hit = hit;
        }

        public Character Target => _target;
        public Weapon Weapon => _weapon;
        public bool Hit => _hit;

        public delegate void OnHitHandler(float dealtDamage);
        public event OnHitHandler OnHit;

        public void HitCharacter()
        {
            if (_hit)
            {
                float damage = _weapon.Descriptor.Damage, dealtDamage = 0;
                
                if (_target.Armor > 0 && damage > 0)
                {
                    var previousValue = _target.Armor.Value;
                    if (previousValue < damage)
                    {
                        _target.Armor.Value -= previousValue;
                        dealtDamage = previousValue - _target.Armor.Value;
                    }
                    else
                    {
                        _target.Armor.Value -= damage;
                        dealtDamage = previousValue - _target.Armor.Value;
                    }
                    damage -= dealtDamage;
                }
                
                if (_target.Health > 0 && damage > 0)
                {
                    var previousValue = _target.Health.Value;
                    _target.Health.Value -= damage;
                    dealtDamage = previousValue - _target.Health.Value;
                }
                
                OnHit?.Invoke(dealtDamage);
            }
            else
            {
                OnHit?.Invoke(0);
            }
        }
    }
}
