namespace Scorewarrior.Test.Models
{
    public abstract class Describable<TDescriptor>
    {
        public TDescriptor Descriptor
        {
            get;
            private set;
        }

        public Describable(TDescriptor descriptor)
        {
            Descriptor = descriptor;
        }
    }
}
