﻿using System;
using Scorewarrior.Test.Descriptors;
using Scorewarrior.Test.Utility;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Scorewarrior.Test.Models
{
	public class Character : ModifiableModel<Character, CharacterDescriptor>
	{
		public enum State
		{
			Idle,
			Aiming,
			Shooting,
			Reloading
		}

		public delegate void OnStateChangedHandler(State state);

		public event OnStateChangedHandler OnStateChanged;
		public event Action OnShoot;
		public event Action OnDeath;
		public event Action OnDestroy;
		
		private readonly Weapon _weapon;
		private readonly Battlefield _battlefield;
		private readonly Transform _transform;

		private State _state;
		private Character _currentTarget;
		private float _time;

		public Character(CharacterDescriptor characterDescriptor, Transform transform, 
			Weapon weapon, Battlefield battlefield) : base(characterDescriptor)
		{
			_weapon = weapon;
			_battlefield = battlefield;
			_transform = transform;
			Health = new Observable<float>(Descriptor.MaxHealth);
			Armor = new Observable<float>(Descriptor.MaxArmor);
			Health.OnChanged += CheckAliveness;
			Armor.OnChanged += CheckAliveness;
		}

		public bool IsAlive => Health > 0 || Armor > 0;

		public Observable<float> Health { get; }
		public Observable<float> Armor { get; }

		public State CharacterState
		{
			get => _state;
			set => SetState(value);
		}

		public Weapon Weapon => _weapon;

		public Transform Transform => _transform;
		public Vector3 Position => _transform.position;

		public void Update(float deltaTime)
		{
			if (IsAlive)
			{
				switch (CharacterState)
				{
					case State.Idle:
						if (_battlefield.TryGetNearestAliveEnemy(this, out Character target))
						{
							_currentTarget = target;
							CharacterState = State.Aiming;
							_time = Descriptor.AimTime;
							_transform.LookAt(_currentTarget.Position);
						}
						break;
					case State.Aiming:
						if (_currentTarget != null && _currentTarget.IsAlive)
						{
							if (_time > 0)
							{
								_time -= deltaTime;
							}
							else
							{
								CharacterState = State.Shooting;
								_time = 0;
							}
						}
						else
						{
							CharacterState = State.Idle;
							_time = 0;
						}
						break;
					case State.Shooting:
						if (_currentTarget != null && _currentTarget.IsAlive)
						{
							if (_weapon.HasAmmo)
							{
								if (_weapon.IsReady)
								{
									float random = Random.Range(0.0f, 1.0f);
									bool hit = random <= Descriptor.Accuracy &&
											random <= _weapon.Descriptor.Accuracy &&
											random >= _currentTarget.Descriptor.Dexterity;
									_weapon.Fire(_currentTarget, hit);
									OnShoot?.Invoke();
								}
								else
								{
									_weapon.Update(deltaTime);
								}
							}
							else
							{
								CharacterState = State.Reloading;
								_time = _weapon.Descriptor.ReloadTime;
							}
						}
						else
						{
							CharacterState = State.Idle;
						}
						break;
					case State.Reloading:
						if (_time > 0)
						{
							_time -= deltaTime;
						}
						else
						{
							if (_currentTarget != null && _currentTarget.IsAlive)
							{
								CharacterState = State.Shooting;
							}
							else
							{
								CharacterState = State.Idle;
							}
							_weapon.Reload();
							_time = 0;
						}
						break;
				}
			}
		}

		public void Destroy()
		{
			OnDestroy?.Invoke();
		}

		private void CheckAliveness(float value)
		{
			if (!IsAlive)
			{
				OnDeath?.Invoke();
			}
		}

		private void SetState(State state)
		{
			_state = state;
			OnStateChanged?.Invoke(_state);
		}
	}
}